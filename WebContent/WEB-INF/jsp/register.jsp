<%--
  Created by IntelliJ IDEA.
  User: George Craciun
  Date: 11/4/2019
  Time: 6:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Form Exercise</title>
    <style type="">
        body {
            background: url(https://cdn.pixabay.com/photo/2018/05/12/19/20/mosaic-3394375_1280.jpg);
            font-size: 20px;
            background-repeat: no-repeat;
            background-size: cover;

        }

        #username {
            font-size: 20px;

        }

        #lastname {
            font-size: 20px;

        }

        #firstname {
            font-size: 20px;
        }

        form {
            text-align: center;
        }
    </style>
</head>
<body>
<form action="/registernow" method="post">
    <h1>Register</h1>
    <p>
        <label class="firstname">First Name:</label>
        <input class="firstname" id="firstname" type="text" name="myfirst" placeholder="John">
    </p>
    <p>
        <label class="lastname">Last Name:</label>
        <input class="lastname" id="lastname" type="text" name="mylast" placeholder="Smith">
    </p>
    <p><label class="username">Username:</label>
        <input class="username" id="username" type="text" name="user1" placeholder="JohnSmith"></p>

    <p>
        <label for="male">Male</label>
        <input id="male" type="radio" name="gender" value="male">
        <label for="female">Female</label>
        <input id="female" type="radio" name="gender" value="female">
        <label for="other">Other</label>
        <input id="other" type="radio" name="gender" value="other">
    </p>
    <p>
        <label for="email">Email:</label>
        <input id="email" type="Email" name="myemail" placeholder="JohnSmith@gmail.com">
    </p>
    <p>
        <label for="password">Password:</label>
        <input id="password" type="password" name="pass" placeholder="your password" pattern=".{8,12}" required
               title="Password must be 8 to 12 characters">

    </p>
    <p>
        <label>Birthday:</label>
        <select name="Month">
            <option>Month</option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="11">Octomber</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
        <select name="Day">
            <option>Day</option>
            <option value="01">1</option>
            <option value="02">2</option>
            <option value="03">3</option>
            <option value="04">4</option>
            <option value="05">5</option>
            <option value="06">6</option>
            <option value="07">7</option>
            <option value="08">8</option>
            <option value="09">9</option>
            <option value="10">10</option>
            <option value="10">11</option>
            <option value="10">12</option>
            <option value="10">13</option>
            <option value="10">14</option>
            <option value="10">15</option>
            <option value="10">16</option>
            <option value="10">17</option>
            <option value="10">18</option>
            <option value="10">19</option>
            <option value="10">20</option>
            <option value="10">21</option>
            <option value="10">22</option>
            <option value="10">23</option>
            <option value="10">24</option>
            <option value="10">25</option>
            <option value="10">26</option>
            <option value="10">27</option>
            <option value="10">28</option>
            <option value="10">29</option>
            <option value="10">30</option>

        </select>
        <select name="Year">
            <option>Year</option>
            <option value="1998">1994</option>
            <option value="1998">1995</option>
            <option value="1998">1996</option>
            <option value="1998">1997</option>
            <option value="1998">1998</option>
            <option value="1998">1999</option>
            <option value="1998">2000</option>
            <option value="1998">2001</option>
            <option value="1998">2002</option>
            <option value="1998">2003</option>
            <option value="1998">2004</option>
        </select>

    </p>
    <p><label for="checkbox">I agree to the terms and conditions</label>
        <input id="checkbox" type="checkbox" name="checkbox" value="Agreed">
    </p>
    <p>
        <button type="submit" class="btn btn-primary btn-lg " value="register">Register</button>
    </p>


</form>

</body>
</html>
