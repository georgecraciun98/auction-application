
package Controller;

import Model.Product;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author manoj.bardhan
 */
@Controller


public class  ControllerClass {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home() {
        System.out.println("salut");
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("validUser", 0);
        return mav;
    }

    @RequestMapping(value = "/login")
    public String goLoginpage() {
        System.out.println("loginhey");
        return "submit";
    }
    @RequestMapping(value = "/register")
    public String goRegisterpage() {
        System.out.println("loginhey");
        return "register";
    }

    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    public String Recover() {
        System.out.println("recover");
        return "RecoverPassword";
    }



    @RequestMapping(value = "/products{category}")
    public ModelAndView gotoElectronics(@RequestParam(value = "category",defaultValue ="0") int cat,@RequestParam(value = "validUser",defaultValue = "0") int userval) {
        int validuser=0;
        ModelAndView mav = new ModelAndView("Products");
        System.out.println(cat);
       if(cat==0)
           cat=2;
       List<Product> myList=Product.listActiveProductsByCategory(cat);

        mav.addObject("lists", myList);
        mav.addObject("validUser", userval);

        return mav;
    }

    @RequestMapping(value = "/about")
    public ModelAndView aboutus() {
        ModelAndView mav = new ModelAndView("About");

        return mav;
    }

    @RequestMapping(value = "/contact")
    public ModelAndView contactus() {
        ModelAndView mav = new ModelAndView("Contact");

        return mav;
    }


}

