package Controller;

import Model.user;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class LogginController {
    private String nume;
    private String parola;

    @RequestMapping(value = "/submit")
    public ModelAndView getUserandPass(
            @RequestParam(name = "loginname") String nume,
            @RequestParam(name = "passwordd") String parola,
            @RequestParam(name = "loggin2") String login
    ) {
        System.out.println("s a facut initializarea");

        if (LogginController.verifica(nume, parola) == 1)
        {
            ModelAndView m=new ModelAndView("index");
            m.addObject("validUser",1);
            return m;}
        else
        {
            ModelAndView m=new ModelAndView("submit");
            m.addObject("validUser",0);
            return m;
           }
    }

    @RequestMapping(value = "/gotocart")
    public ModelAndView getUserandPass1(

    ) {

        ModelAndView m=new ModelAndView("Cart");

        return m;

    }

    @RequestMapping(value = "/registernow")
    public ModelAndView goRegisterpage(@RequestParam(name = "myfirst") String firstname,
                                       @RequestParam(name = "mylast") String lastname,
                                       @RequestParam(name = "user1") String username,
                                       @RequestParam(name = "gender") String gender,
                                       @RequestParam(name = "myemail") String email,
                                       @RequestParam(name = "pass") String password,
                                       @RequestParam(name = "Month") int month,
                                       @RequestParam(name = "Day") int day,
                                       @RequestParam(name = "Year") int year
    ) {
        String month1="";
        String day1="";
        if (month<10){
            month1="0"+month;

        }
        else
            month1=""+month;
        if (day<10){
            day1="0"+day;

        }
        else
            day1=""+day;

        String date = "" + year + "" + month1 + "" + day1;
        System.out.println(date);
        user user1=new user();
        if (user1.registerUser(firstname,lastname,username,gender,email,password,date) == true)
        {
            ModelAndView m=new ModelAndView("index");
            m.addObject("validUser",1);
            return m;}
        else
        {
            ModelAndView m=new ModelAndView("submit");

            return m;
        }



    }

    public static int verifica(String Username, String Parola) {
        user x = new user();
        if (x.isValidUserCredentials(Username, Parola)==true)
            return 1;
        else
            return 0;
    }
}
