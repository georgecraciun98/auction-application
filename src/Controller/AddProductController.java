package Controller;

import Model.Product;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
public class AddProductController {

    @RequestMapping(value = "/gotoproduct")
    public String addProdpage(  ) {

        return "gotoproduct";
    }


    @RequestMapping(value = "/addproduct")
    public ModelAndView getProduct(@RequestParam(name = "categorie") String categorie,
                                   @RequestParam(name = "descriere") String descriere,
                                   @RequestParam(name = "poza") String poza,
                                   @RequestParam(name = "pretdepornire") int pret
    ) throws IOException {
        int categorie1=1;
        Product p=new Product(0,1,descriere,pret,poza);
        if(categorie.equals("Electronics"))
            categorie1=1;
        else if(categorie.equals("Fashion"))
            categorie1=2;
        else if(categorie.equals("Auto Parts"))
            categorie1=3;
        else if(categorie.equals("Sports"))
            categorie1=4;
        else if(categorie.equals("Collectibles&Art"))
            categorie1=5;
        else if(categorie.equals("Health&Beauty"))
            categorie1=6;
        else if(categorie.equals("Home&Garden"))
            categorie1=7;
        if (p.registerProduct(0,categorie1,descriere,pret,poza)==true){
            ModelAndView m=new ModelAndView("index");
            return m;
        }

        ModelAndView m=new ModelAndView("gotoproduct");
        return m;
    }


}