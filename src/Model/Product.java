package Model;

import Dao.DB_user;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Product {
    private int id;
    private int categorie;
    private String descriere;
    private int pret;
    private byte[] poza;
    private String url;
    boolean isValidProduct=false;
   static Connection con= DB_user.getConnection();
   Statement stmt=null;
    public Product(int a,int b,String c,int d,String e) {
        id=a;
        categorie=b;
        descriere=c;
        pret=d;
        url=e;
    }
    public boolean registerProduct(int id,int categorie,
                                   String descriere,int pret,String url){


        String sql = "";
        isValidProduct = false;
        System.out.println("We are checking credentials");
        try {

            sql = "insert into auction_application.auction_items(" +
                    "id_items,item_category,item_status_code,item_name,asking_price,date_submitted,details,seller_id,image_url) " +
                    "values(34,"+
                      + categorie + ",2,\""+descriere+"\","+pret+"," + "cast(sysdate() as date)" + ",\"" + descriere   + "\",1,\""+url +"\" );";
            Connection conn=DB_user.getConnection();
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            System.out.println(sql);
            preparedStmt.execute();

            return true;

        } catch (SQLException s) {
            s.printStackTrace();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Closing DBConnection");

        return isValidProduct;
    }
    public List<Product> listActiveProducts() {
        List<Product> myList=new ArrayList<>();
        String selectActiveProducts = "SELECT * FROM Product ";
        try {
            System.out.println("Creating Statement");
            stmt = con.createStatement();
            System.out.println("Am ajuns sa creem statement");
            System.out.println(selectActiveProducts);
            ResultSet rs = stmt.executeQuery(selectActiveProducts);
            while (rs.next()){
            Product m=new Product(rs.getInt("id_items"),rs.getInt("item_category"),rs.getString("item_name")
            ,rs.getInt("asking_price"),rs.getString("image_url"));
            System.out.println(m.getUrl());
            }
            rs.close();
            stmt.close();

        } catch (SQLException s) {
            s.printStackTrace();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Closing DBConnection");
       return myList;
    }

    public static List<Product> listActiveProductsByCategory(int categoryId) {
        List<Product> myList=new ArrayList<>();
        String selectActiveProducts = "SELECT * FROM auction_items where item_category="+categoryId+"";
        try {
            System.out.println("Am ajuns sa creem statement");

            System.out.println("Creating Statement");
            Statement stmt1 = con.createStatement();

            System.out.println(selectActiveProducts);
            ResultSet rs = stmt1.executeQuery(selectActiveProducts);
            while (rs.next()){
                Product m=new Product(rs.getInt("id_items"),rs.getInt("item_category"),rs.getString("item_name")
                        ,rs.getInt("asking_price"),rs.getString("image_url"));
                myList.add(m);
                System.out.println("Acesta este url ul nostru ! ");
                System.out.println(m.getUrl());
            }
            rs.close();
            stmt1.close();

        } catch (SQLException s) {
            s.printStackTrace();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Closing DBConnection");
        return myList;
    }
    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public String getDescriere() {
        return descriere;
    }
    public String getUrl(){
        return url;
    }
    public int getCategorie() {
        return categorie;
    }

    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }


    public byte[] getPoza() {
        return poza;
    }

    public void setPoza(byte[] poza) {
        this.poza = poza;
    }
}
