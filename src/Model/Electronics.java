package Model;

public class Electronics {
    private String nume;
    private String url;
    private String model;
    private String descriere;

    public Electronics() {
    }

    public Electronics(String nume, String url, String model, String descriere) {
        this.nume = nume;
        this.url = url;
        this.model = model;
        this.descriere = descriere;
    }


    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }
}
