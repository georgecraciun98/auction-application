package Model;

import Dao.DB_user;

public class user {
    public boolean isValidUserCredentials(String userName,
                                          String userPassword
    ) {
        DB_user userObj = new DB_user();
        return userObj.isValidUserLogin(userName, userPassword);
    }

    public boolean registerUser(String FirstName, String LastName,
                                String username, String gender,
                                String email, String password, String date) {
        DB_user userObj = new DB_user();

        return userObj.RegisterUser(FirstName,LastName,username,gender,email,password,date);
    }

}
