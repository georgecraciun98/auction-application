<%--
  Created by IntelliJ IDEA.
  User: George Craciun
  Date: 12/1/2019
  Time: 9:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
    <script src="https://kit.fontawesome.com/bd669c2413.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alatsi&display=swap" rel="stylesheet">

    <link href="/Styling/electronicsStyle.css" rel="stylesheet">
    <title> Shop By Category</title>
    <style>

        #centered {
            text-align: center;
            padding-top: 25%

        }

        .col-lg-4, .col-md-6 {
            height: 30%;
        }

        hr {
            width: 400px;
            border-top: 1px solid #f8f8f8;
            border-bottom: 1px solid rgb(0, 0, 0);
        }

        p {
            text-align: center;
            font-family: 'Open Sans', sans-serif;
            color: rgb(67, 80, 250);
        }

        .elem {
            height: 350px;
            border-bottom: 30px;

        }

        .container {
            margin-left: 10%;
            margin-right: 10%;


        }

        .c1 {
            background-color: white;
            height: 100%;
            padding-bottom: 40px;
        }

        .thumbnail {
            padding: 0px;
            margin: 0px;
            height: 80%;

        }

        .thumbnail img {
            max-height: 100%;
            max-width: 100%;
        }

        .textzone p {

            font-size: 16px;
            font-family: 'Alatsi', sans-serif;
            color: black;

        }

        body {
            background: url(https://cdn.pixabay.com/photo/2014/09/24/14/29/mac-459196_1280.jpg);
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            font-family: 'Open Sans', sans-serif;
            color: white;
        }

        html {
            height: 100%;
        }

        h1 {
            font-weight: 700;
            font-size: 5em;
        }
    </style>
</head>

<body>


<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><i class="fas fa-money-bill-alt"></i></i>Gold Lion</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Categories <span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        <li><a href="/products?category=1&validUser=${validUser}" method="get" >Electronics</a></li>
                        <li><a href="/products?category=2&validUser=${validUser}" method="get">Fashion</a></li>
                        <li><a href="/products?category=3&validUser=${validUser}" method="get">Auto Parts</a></li>
                        <li><a href="/products?category=4&validUser=${validUser}" method="get">Sports</a></li>
                        <li><a href="/products?category=5&validUser=${validUser}" method="get">Collectibles&Art</a></li>
                        <li><a href="/products?category=6&validUser=${validUser}" method="get">Health&Beauty</a></li>
                        <li><a href="/products?category=7&validUser=${validUser}" method="get">Home&Garden</a></li>

                    </ul>
                </li>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>

            <c:if test = "${validUser != 1}">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login" method="get"><i class="fas fa-user-shield"></i>Login</a></li>
                    <li><a href="/register" method="get"> <i class="fas fa-address-card"></i> Sign up</a></li>



                </ul>
            </c:if>
            <c:if test = "${validUser == 1}">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="${pageContext.request.contextPath}/gotoproduct" method="post"><i
                            class="fas fa-user-shield"></i>AddProduct From Userpage</a></li>
                    <li> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cart">Cart (<span class="total-count"></span>)</button><button class="clear-cart btn btn-danger">Clear Cart</button>
                    </li>

                 </ul>



            </c:if>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <c:if test="${not empty lists}">


        <c:forEach var="listValue" items="${lists}">
        <div class="col-lg-4 col-md-6 elem" >

           <div class="c1">

               <div class="thumbnail ">
                  <img src="${listValue.url}" >
               </div>
               <div class="textzone">
                   <p> ${listValue.descriere}</p>
                   <p> ${listValue.pret}</p>
                   <a href="#" data-name="${listValue.descriere}" data-price="${listValue.pret}" class="add-to-cart btn btn-primary">Add to cart</a>
               </div>

            </div>
        </div>
        </c:forEach>
        </c:if>


</div>

<p11>

    <!-- Nav -->


    <!-- Main -->

    <!-- Modal -->
    <div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cart</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span >&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="show-cart table">

                    </table>
                    <div style='color: black'>Total price: $<span class="total-cart"></span></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Order now</button>
                </div>
            </div>
        </div>
    </div>


</p11>

</body>
<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script>
    // **********************************************
    // Shopping Cart API
    // **********************************************

    var shoppingCart = (function() {
        // =============================
        // Private methods and propeties
        // =============================
        cart = [];

        // Constructor
        function Item(name, price, count) {
            this.name = name;
            this.price = price;
            this.count = count;
        }

        // Save cart
        function saveCart() {
            sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
        }

        // Load cart
        function loadCart() {
            cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
        }
        if (sessionStorage.getItem("shoppingCart") != null) {
            loadCart();
        }


        // =============================
        // Public methods and propeties
        // =============================
        var obj = {};

        // Add to cart
        obj.addItemToCart = function(name, price, count) {
            for(var item in cart) {
                if(cart[item].name === name) {
                    cart[item].count ++;
                    saveCart();
                    return;
                }
            }
            var item = new Item(name, price, count);
            cart.push(item);
            saveCart();
        }
        // Set count from item
        obj.setCountForItem = function(name, count) {
            for(var i in cart) {
                if (cart[i].name === name) {
                    cart[i].count = count;
                    break;
                }
            }
        };
        // Remove item from cart
        obj.removeItemFromCart = function(name) {
            for(var item in cart) {
                if(cart[item].name === name) {
                    cart[item].count --;
                    if(cart[item].count === 0) {
                        cart.splice(item, 1);
                    }
                    break;
                }
            }
            saveCart();
        }

        // Remove all items from cart
        obj.removeItemFromCartAll = function(name) {
            for(var item in cart) {
                if(cart[item].name === name) {
                    cart.splice(item, 1);
                    break;
                }
            }
            saveCart();
        }

        // Clear cart
        obj.clearCart = function() {
            cart = [];
            saveCart();
        }

        // Count cart
        obj.totalCount = function() {
            var totalCount = 0;
            for(var item in cart) {
                totalCount += cart[item].count;
            }
            return totalCount;
        }

        // Total cart
        obj.totalCart = function() {
            var totalCart = 0;
            for(var item in cart) {
                totalCart += cart[item].price * cart[item].count;
            }
            return Number(totalCart.toFixed(2));
        }

        // List cart
        obj.listCart = function() {
            var cartCopy = [];
            for(i in cart) {
                item = cart[i];
                itemCopy = {};
                for(p in item) {
                    itemCopy[p] = item[p];

                }
                itemCopy.total = Number(item.price * item.count).toFixed(2);
                cartCopy.push(itemCopy)
            }
            return cartCopy;
        }

        // cart : Array
        // Item : Object/Class
        // addItemToCart : Function
        // removeItemFromCart : Function
        // removeItemFromCartAll : Function
        // clearCart : Function
        // countCart : Function
        // totalCart : Function
        // listCart : Function
        // saveCart : Function
        // loadCart : Function
        return obj;
    })();


    // ***************************************
    // Triggers / Events
    // ***************************************
    // Add item
    $('.add-to-cart').click(function(event) {
        event.preventDefault();
        var name = $(this).data('name');
        var price = Number($(this).data('price'));
        shoppingCart.addItemToCart(name, price, 1);
        displayCart();
    });

    // Clear items
    $('.clear-cart').click(function() {
        shoppingCart.clearCart();
        displayCart();
    });


    function displayCart() {
        var cartArray = shoppingCart.listCart();
        var output = "";
        for(var i in cartArray) {
            output += "<tr>"
                + "<td style='color: black'>" + cartArray[i].name + "</td>"
                + "<td style='color: black'>(" + cartArray[i].price + ")</td>"
                + "<td style='color: black'><div class='input-group'><button class='minus-item input-group-addon btn btn-primary' data-name=" + cartArray[i].name + ">-</button>"
                + "<input style='color: black' type='number' class='item-count form-control' data-name='" + cartArray[i].name + "' value='" + cartArray[i].count + "'>"
                + "<button style='color: black' class='plus-item btn btn-primary input-group-addon' data-name=" + cartArray[i].name + ">+</button></div></td>"
                + "<td style='color: black'><button class='delete-item btn btn-danger' data-name=" + cartArray[i].name + ">X</button></td>"
                + " = "
                + "<td style='color: black'>" + cartArray[i].total + "</td>"
                +  "</tr>";
        }
        $('.show-cart').html(output);
        $('.total-cart').html(shoppingCart.totalCart());
        $('.total-count').html(shoppingCart.totalCount());
    }

    // Delete item button

    $('.show-cart').on("click", ".delete-item", function(event) {
        var name = $(this).data('name')
        shoppingCart.removeItemFromCartAll(name);
        displayCart();
    })


    // -1
    $('.show-cart').on("click", ".minus-item", function(event) {
        var name = $(this).data('name')
        shoppingCart.removeItemFromCart(name);
        displayCart();
    })
    // +1
    $('.show-cart').on("click", ".plus-item", function(event) {
        var name = $(this).data('name')
        shoppingCart.addItemToCart(name);
        displayCart();
    })

    // Item count input
    $('.show-cart').on("change", ".item-count", function(event) {
        var name = $(this).data('name');
        var count = Number($(this).val());
        shoppingCart.setCountForItem(name, count);
        displayCart();
    });

    displayCart();

</script>
</html>